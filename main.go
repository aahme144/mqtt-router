package main

import (
	"flag"
	"github.com/fhmq/router/server"

	log "github.com/cihub/seelog"
)

const (
	LOG_CONFIG_FILE_NAME = "./conf/log.xml"
)

var port string

func init() {

	flag.StringVar(&port, "p", "9888", "port must be a number")

	logger, err := log.LoggerFromConfigAsFile(LOG_CONFIG_FILE_NAME)
	if err != nil {
		panic(err)
	}
	log.ReplaceLogger(logger)

}
func main() {
	flag.Parse()
	topic := "broker000100101info"
	srv := server.NewServer(port, topic)
	srv.Start()
}
