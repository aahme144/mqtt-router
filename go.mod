module gitlab.com/aahme144/mqtt-router

go 1.12

require (
	github.com/bitly/go-simplejson v0.5.0
	github.com/bmizerany/assert v0.0.0-20160611221934-b7ed37b82869 // indirect
	github.com/cihub/seelog v0.0.0-20170130134532-f561c5e57575
	github.com/eclipse/paho.mqtt.golang v1.2.0
	github.com/fhmq/router v0.0.0-20180201071730-06c291af1cf5
	github.com/kr/pretty v0.2.1 // indirect
)
