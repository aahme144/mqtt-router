
FROM golang:1.14 as builder
WORKDIR /User/container/mqtt-build
ARG goos=linux
ARG goarch=amd64
ARG tag=latest
RUN go mod init mqtt-build &&\
    go get gitlab.com/aahme144/mqtt-router@$tag
ENV GOOS=$goos \
    GOARCH=$goarch \
    CGO_ENABLED=0
RUN cd  "$(\ls -1dt $GOPATH/pkg/mod/gitlab.com/aahme144//*/ | head -n 1)" &&\
    cp -r "$(pwd)"/conf /User/container/mqtt-build/conf &&\
    go build -o /User/container/mqtt-build/mqtt-router -a -ldflags '-extldflags "-static"' .


FROM registry:2.7.1
WORKDIR /
COPY --from=builder /User/container/mqtt-build/mqtt-router /bin/mqtt-router
COPY --from=builder /User/container/mqtt-build/conf ./conf
RUN chmod +x /bin/mqtt-router
EXPOSE 9888

ENTRYPOINT ["mqtt-router"]
CMD ["-p", "9887"]